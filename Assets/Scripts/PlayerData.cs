using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
    public int level;
    public int health;
    public int speed;

    public PlayerData(int level, int health, int speed)
    {
        this.level = level;
        this.health = health;
        this.speed = speed;
    }
}
